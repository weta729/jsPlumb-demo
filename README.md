![输入图片说明](https://images.gitee.com/uploads/images/2019/0323/211837_328ce9f5_2017165.png "屏幕截图.png")

 **什么也不想说。看图看效果。** 

*  **1. 节点添加** 

       （1）添加图形

       （2）在xml中增加位置节点以及流程节点

       （3）节点拖动。更新xml中的位置信息

       （4）节点拖动，影响到了连线的坐标信息。


*  **2、节点删除** 

      （1）删除xml中的位置信息以及流程节点

      （2）删除xml中该节点相关的所有连线节点以及连线坐标。

      （3）删除html图形节点

      （4）删除该节点对应的端点以及所有连线。
    

* **  **3、添加连线** 

       jsplumb监听connection事件

      （1）在xml的process节点中增加连线节点。

      （2）在xml中添加连线的位置信息。


*  **4、连线删除** 

       jsplumb监听connectionDetached事件

      （1）删除xml中的连线节点

      （2）删除xml中连线的位置信息。

*  **5、连线拖动** 

       jsplumb监听connectionDragEnd事件

      （1）更新xml中连线节点信息以及位置信息。



```
            //删除节点
            this.$el.on("click", ".delete",function () {
                var $this = $(this);
                $.modal.confirm('确定要删除该节点吗',function(){
                    jsPlumb.removeAllEndpoints($this.parent().attr("id"));
                    //$this.parent().remove();

					let $node = $this.parent();
					let id = $node.attr("id");

					// 删除到该节点的所有连接
				   jsPlumb.select({source:id}).each(function(conn){
					   //var flow = getSequenceFlow(conn);
					   //removeNodeOrFlow("sequenceFlow",$(flow).attr("id"));
					   jsPlumb.detach(conn);
				   });
				   jsPlumb.select({target:id}).each(function(conn){
					   //var flow = getSequenceFlow(conn);
					   //removeNodeOrFlow("sequenceFlow",$(flow).attr("id"));
					   jsPlumb.detach(conn);
				   });
				   $node.remove();
                });
            });
```
